select case
                when b.company_size is null then 'unknown'
                when b.company_size<=10 then '1-10'
                when b.company_size between 11 and 50 then '11-50'
                when b.company_size between 51 and 500 then '51-500'
                when b.company_size between 501 and 1000 then '501 - 1000'
                when b.company_size > 1000 then '1001+'
        end as company_size,
count(distinct a.tech_email_domain)
from license as a
left join playground_bizops.customer_size as b on a.tech_email_domain = b.email_domain
where a.expiry_date > '2016-06-01'
and a.platform = 'Server'
and a.license_level = 'Full'
and a.tech_email_domain ilike '%.gov%'
group by 1

