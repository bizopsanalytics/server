--Server customer's who were New to New between 01/09/14 and 31/12/14 (FY15 Q2) and only purchased 1 product on their 'land' day.
with first_date as
(
select  email_domain, 
        sen,
        base_product,
        sold_via_partner,
        country,
        region,
        user_limit,
        min(date) as min_date
from    sale 
where   sale_type = 'New to New' 
and     platform = 'Server'
group by 1,2,3,4,5,6,7
),
cohort as
(
--create cohort within dates, platform and product requirements
select  a.email_domain,
        a.sen,
        b.base_product,
        a.sold_via_partner,
        a.country,
        a.region,
        a.user_limit,
        b.date as min_date
from    first_date as a
left join sale as b on a.email_domain = b.email_domain
where   a.min_date between '2014-09-01' and '2014-12-31'
and     b.date = a.min_date
and     b.platform = 'Server'
and     b.base_product in ('JIRA','Confluence', 'Bamboo', 'HipChat', 'Bitbucket', 'Crucible', 'FishEye')
order by 1,3
)
, final_cohort as (
-- view cohort
select  a.email_domain, 
        a.sen, 
        a.sold_via_partner,
        a.region,
        a.country,
        a.user_limit,
        a.base_product,
        a.min_date
from cohort as a
left join 
        (       --find out how many products the customer bought on their first day
                select distinct a.email_domain, 
                count(b.base_product) as prod_count 
                from first_date as a
                left join sale as b on a.email_domain = b.email_domain
                where   a.min_date between '2014-09-01' and '2014-12-31'
                and     b.date = a.min_date
                and     b.platform = 'Server'
                group by 1
                ) as b on a.email_domain = b.email_domain
where   b.prod_count = 1
group by 1,2,3,4,5,6,7,8
)
select  a.sen, 
        a.user_limit,
        b.tech_email_domain,
        b.sen as other_sen,
        b.user_limit as other_limit,
        b.platform,
        count(b.sen) as instances_of_prod
from final_cohort as a
left join license as b on a.email_domain = b.tech_email_domain and a.base_product = b.base_product
where b.expiry_date > (a.min_date + 364)
and b.license_level = 'Full'
and a.sen <> cast(b.sen as text)
group by 1,2,3,4,5,6
