--identify the date a customer was first acquired on Server.
-- full licenses only
-- split by direct vs expert sales
with first_date as
(
select
        distinct tech_email_domain,
  
case
        when min(start_date) < '2011-07-01' then 'Before_FY12'
        when min(start_date) between '2011-07-01' and '2012-06-30' then 'FY12'
        when min(start_date) between '2012-07-01' and '2013-06-30' then 'FY13'
        when min(start_date) between '2013-07-01' and '2014-06-30' then 'FY14'
        when min(start_date) between '2014-07-01' and '2014-09-30' then 'FY15_Q1'
        when min(start_date) between '2014-10-01' and '2014-12-31' then 'FY15_Q2'
        when min(start_date) between '2015-01-01' and '2015-03-31' then 'FY15_Q3'
        when min(start_date) between '2015-04-01' and '2015-06-30' then 'FY15_Q4'
        when min(start_date) between '2015-07-01' and '2015-09-30' then 'FY16_Q1'
        when min(start_date) between '2015-10-01' and '2015-12-31' then 'FY16_Q2'
        when min(start_date) between '2016-01-01' and '2016-03-31' then 'FY16_Q3'
        when min(start_date) between '2016-04-01' and '2016-06-30' then 'FY16_Q4'
end as start_period
from license 
where platform = 'Server'
and license_level = 'Full'
group by tech_email_domain

) 

select a.start_period, 
case
when c.sold_via_partner = 'True' then 'Expert'
when c.sold_via_partner = 'False' then 'Direct'
end as sale_type,
count(distinct a.tech_email_domain)
from first_date as a
join license as b on a.tech_email_domain = b.tech_email_domain
join sale as c on cast(b.sen as text) = c.sen
group by 1,2



