--Downgraded to Starter with same sen
with full_group as
(
select          sen, min(date) as min_date
                from sale
                where platform = 'Server'
                and license_level = 'Full'
                --and financial_year > 'FY2014'
group by 1
)
select b.financial_year, count(distinct a.sen)
from full_group as a
left join sale as b on a.sen = b.sen
where b.date > a.min_date
and b.license_level = 'Starter'
group by 1