--How many customer's own more than 1 instance of a product?
with first_date as
(
select  email_domain, 
        sen,
        platform,
        base_product,
        quarter,
        min(date) as min_date
from    sale 
where base_product in ('JIRA', 'JIRA Software', 'JIRA Service Desk', 'Confluence', 'Bamboo', 'HipChat', 'Bitbucket', 'Crucible', 'FishEye')
group by 1,2,3,4,5
)
select  a.quarter,
        a.base_product,
        count(distinct b.tech_email_domain) as num_other_instance
from first_date as a
left join license as b on a.email_domain = b.tech_email_domain and a.base_product = b.base_product
where b.license_level = 'Full'
and expiry_date > a.min_date
and a.sen <> cast(b.sen as text)
group by 1,2
