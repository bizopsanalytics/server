--company size
select
case
        when b.company_size is null then 'unknown'
        when b.company_size<=10 then '1-10'
        when b.company_size between 11 and 50 then '11-50'
        when b.company_size between 51 and 500 then '51-500'
        when b.company_size > 500 then '500+'
end as company_size, 
count(a.email_domain)
from playground_bizops.server_cohort_993 as a
left join playground_bizops.customer_size as b on a.email_domain = b.email_domain
group by 1
order by 1

--sales channel
select b.sold_via_partner,
count(distinct a.email_domain)
from playground_bizops.server_cohort_993 as a
left join sale as b on a.email_domain = b.email_domain
group by 1
order by 1

--country
select b.country,
count(distinct a.email_domain)
from playground_bizops.server_cohort_993 as a
left join sale as b on a.email_domain = b.email_domain
group by 1
order by 1