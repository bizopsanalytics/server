-- cohort 1 upgraded
select case
 when company_size is null then 'unknown'
 when company_size<=10 then '1-10'
 when company_size between 11 and 50 then '11-50'
 when company_size between 51 and 500 then '51-500'
 when company_size > 500 then '501+'
end as company_size,
 count(distinct sen)
from sale as a
left join playground_bizops.customer_size as b on a.email_domain = b.email_domain
where sen in (
'4193328',
'3849818',
'4197982',
'3740158',
'4081994',
'4169858',
'4232542',
'4199308',
'3898234',
'3784059',
'4128178',
'3753534',
'3886861',
'3985351',
'3817645',
'3833918',
'4127373',
'3812186',
'3839444',
'3755790',
'4278155',
'4251707',
'4088968',
'3984691',
'4027157',
'4068643',
'4078615',
'3784569',
'4186853',
'3748043',
'3836945',
'4287048',
'3968152',
'3826514',
'3800091',
'4190322',
'3787653',
'3979169',
'4083259',
'4010978',
'3895670')
group by 1
order by 1