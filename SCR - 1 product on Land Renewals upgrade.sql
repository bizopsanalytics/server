-- check renewal rate
--Server customer's who were New to New between 01/09/14 and 31/12/14 (FY15 Q2) and only purchased 1 product on their 'land' day.
with first_date as
(
select  email_domain, 
        sen,
        base_product,
        sold_via_partner,
        country,
        min(date) as min_date
from    sale 
where   sale_type = 'New to New' 
and     platform = 'Server'
group by 1,2,3,4,5
),
cohort as
(
--create cohort within dates, platform and product requirements
select  a.email_domain,
        a.sen,
        b.base_product,
        a.sold_via_partner,
        a.country,
        b.date as min_date
from    first_date as a
left join sale as b on a.email_domain = b.email_domain
where   a.min_date between '2014-09-01' and '2014-12-31'
and     b.date = a.min_date
and     b.platform = 'Server'
and     b.base_product in ('JIRA','Confluence', 'Bamboo', 'Bitbucket', 'Crucible', 'FishEye')
order by 1,3
)
-- final cohort creation
, final_cohort as
(
select  a.email_domain, 
        a.sen, 
        a.sold_via_partner,
        a.country,
        a.base_product,
        a.min_date
from cohort as a
left join 
        (       --find out how many products the customer bought on their first day
                select distinct a.email_domain, 
                count(b.base_product) as prod_count 
                from first_date as a
                left join sale as b on a.email_domain = b.email_domain
                where   a.min_date between '2014-09-01' and '2014-12-31'
                and     b.date = a.min_date
                and     b.platform = 'Server'
                group by 1
                ) as b on a.email_domain = b.email_domain
where   b.prod_count = 1
)
select a.*
from final_cohort as a 
left join license_renewal as b on a.sen = b.sen
where b.renewal_purchase_date >= a.min_date
and existing_start_date > a.min_date 
and existing_start_date <= (a.min_date + 364)
and starter = 'false'
group by 1,2,3,4,5,6;

