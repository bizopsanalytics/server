-- Server Licenses that have had a chance to churn/renew
select  count (distinct sen)
from license
where platform = 'Server'
and start_date < '2015-06-15'
and license_level = 'Full'

--Server licenses that havent had a chance to churn
select count (distinct sen)
from license
where platform = 'Server'
and start_date between '2015-06-15' and '2016-06-15'
and license_level = 'Full'

--upgrade rate of current first year licenses
with first_year as
(
select sen
from license
where platform = 'Server'
and start_date between '2015-06-15' and '2016-06-15'
and license_level = 'Full'
)
select a.sen 
from first_year as a
left join sale as b on cast(a.sen as text) = b.sen 
where sale_type = 'Upgrade'

--number of sens that have renewed since reaching 1 year
with first_sen as 
(
select  sen, 
        min(date)
from sale
where platform = 'Server'
and date < '2015-06-15'
and license_level = 'Full'
group by 1
),
sen_renewal as 
(
select  a.sen,
        count(b.sen) as Renewals
from first_sen as a
left join sale as b on a.sen = b.sen
where b.sale_type = 'Renewal'
group by 1
)
select  Renewals, 
        count(sen)
from sen_renewal
group by 1
order by 1 asc

--did those that churn after their first year upgrade during their first year?
with first_sen as 
(
select  sen, 
        min(date) as min_date
from sale
where platform = 'Server'
and date < '2015-06-15'
and license_level = 'Full'
group by 1
)
select a.sen
from first_sen as a
left join sale as b on a.sen = b.sen
where a.sen not in
        (
        select  a.sen
        from first_sen as a
        left join sale as b on a.sen = b.sen
        where b.sale_type = 'Renewal'
        group by 1
        )
and sale_type = 'Upgrade'
and b.date < a.min_date + 365

--first year renewals based on customers acquire date
with first_purchase as 
(
select  email_domain, 
        financial_year,
        quarter,
        min(date) as min_date
from sale
where platform = 'Server'
and date < '2015-06-15'
and license_level = 'Full'
group by 1,2,3
),
products as 
(
select  a.email_domain,
        a.quarter,
        a.financial_year,
        b.sen,
        min(b.date)
from first_purchase as a
left join sale as b on a.email_domain = b.email_domain
where b.date < a.min_date + 365
group by 1,2,3,4
),
sen_renewal as 
(
select  a.email_domain,
        a.quarter,
        a.financial_year,
        b.sen
from products as a
left join sale as b on a.sen = b.sen
where b.sale_type = 'Renewal'
group by 1,2,3,4
)
select  a.quarter,
        count(distinct b.email_domain) as total_cust, 
        count(distinct c.sen) as total_products,
        count(distinct a.sen) as total_renew
from sen_renewal as a
join first_purchase as b on a.email_domain = b.email_domain
join products as c on a.email_domain = c.email_domain
group by 1

