-- What sized customers are attracted by experts/direct sales channels?
--identify the date a customer was first acquired on Server. Make a count.
--identify the number that are left at a specific point in time.
--Full licenses only
--Server only

with first_date as
(
select
        distinct a.tech_email_domain, c.country,
        
case
        when c.sold_via_partner = 'True' then 'Expert'
        when c.sold_via_partner = 'False' then 'Direct'
        end as sale_type,
        
case
        when d.company_size is null then 'unknown'
        when d.company_size<=10 then '1-10'
        when d.company_size between 11 and 50 then '11-50'
        when d.company_size between 51 and 500 then '51-500'
        when d.company_size > 500 then '500+'
        end as company_size,        

case
        when min(start_date) < '2011-07-01' then 'Before_FY12'
        when min(start_date) between '2011-07-01' and '2012-06-30' then 'FY12'
        when min(start_date) between '2012-07-01' and '2013-06-30' then 'FY13'
        when min(start_date) between '2013-07-01' and '2014-06-30' then 'FY14'
        when min(start_date) between '2014-07-01' and '2014-09-30' then 'FY15_Q1'
        when min(start_date) between '2014-10-01' and '2014-12-31' then 'FY15_Q2'
        when min(start_date) between '2015-01-01' and '2015-03-31' then 'FY15_Q3'
        when min(start_date) between '2015-04-01' and '2015-06-30' then 'FY15_Q4'
        when min(start_date) between '2015-07-01' and '2015-09-30' then 'FY16_Q1'
        when min(start_date) between '2015-10-01' and '2015-12-31' then 'FY16_Q2'
        when min(start_date) between '2016-01-01' and '2016-03-31' then 'FY16_Q3'
        when min(start_date) between '2016-04-01' and '2016-06-30' then 'FY16_Q4'
end as start_period
from license as a
left join       (select email_domain, sold_via_partner, country, min(date) as min_date 
                from sale 
                where platform = 'Server'
                and license_level = 'Full'
                group by 1,2,3
                ) as c on a.tech_email_domain = c.email_domain
left join       (select email_domain, 
                company_size
                from playground_bizops.customer_size 
                ) as d on a.tech_email_domain = d.email_domain
where platform = 'Server'
and license_level = 'Full'
group by 1,2,3,4
) 
, cohort_count as
(
select a.start_period, a.sale_type, a.company_size, a.country,
count(distinct a.tech_email_domain) as cust_count
from first_date as a
group by 1,2,3,4
)
, expertdirect as
(
select a.start_period, a.sale_type, a.company_size, a.country,
count(distinct a.tech_email_domain) as directexpert_count
from first_date as a
left join license as b on a.tech_email_domain = b.tech_email_domain
where b.platform = 'Server'
and b.license_level = 'Full'
and b.expiry_date > '2016-05-22'
group by 1,2,3,4
order by 1
)
select a.start_period, a.sale_type, a.company_size, a.country, a.cust_count, b.directexpert_count
from cohort_count as a
left join expertdirect as b     on a.start_period = b.start_period 
                                and a.sale_type = b.sale_type 
                                and a.company_size = b.company_size
                                and a.country = b.country

