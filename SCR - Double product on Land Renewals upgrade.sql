-- first sen upgraded
with min_dater as
( select sen, min(date) as min_date
from sale
where platform = 'Server'
and license_level = 'Full'
group by 1
)
select a.*
from playground_bizops.scr_double_land_cohort as a 
left join license_renewal as b on a.sen_1 = b.sen
left join min_dater as c on a.sen_1=c.sen
where b.renewal_purchase_date >= c.min_date
and existing_start_date > c.min_date 
and existing_start_date <= (c.min_date + 364)
and starter = 'false'
group by 1,2,3,4

-- second sen upgraded
with min_dater as
( select sen, min(date) as min_date
from sale
where platform = 'Server'
and license_level = 'Full'
group by 1
)
select a.*
from playground_bizops.scr_double_land_cohort as a 
left join license_renewal as b on a.sen_2 = b.sen
left join min_dater as c on a.sen_2=c.sen
where b.renewal_purchase_date >= c.min_date
and existing_start_date > c.min_date 
and existing_start_date <= (c.min_date + 364)
and starter = 'false'
group by 1,2,3,4
