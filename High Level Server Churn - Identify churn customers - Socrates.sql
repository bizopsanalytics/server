--change date id to create focus cohort

with active_dates as
( 
select  a.customer_id,
        a.date_id
from    model.fact_license_active as a
        join model.dim_license as b on a.license_id = b.license_id
        join model.dim_product as c on a.product_id = c.product_id
where c.platform = 'Server'
and b.level = 'Full'
and a.date_id in (20150630)
),
retain_group as
(
select  b.date_id, 
        a.customer_id,
        1 as retained
from    active_dates as a
        join model.fact_license_active as b on a.customer_id = b.customer_id
        join model.dim_license as c on b.license_id = c.license_id
        join model.dim_product as d on b.product_id = d.product_id
where b.date_id in (20150930)
group by 1,2
order by 1
)

select distinct a.customer_id, c.smart_domain, 
case when c.smart
from active_dates as a
left join retain_group as b on a.customer_id = b.customer_id
left join model.dim_customer as c on a.customer_id = c.customer_id
where b.retained is null
order by 1

