--identify the date a customer was first acquired on Server.
--Find out whether they have an expiry date that is greater than start_date + 365 to see whether they have renewed.

with first_date as
(
select
        tech_email_domain, 
        --base_product, 
        --sen,
        --min(start_date) as min_date,
case
        when min(start_date) < '2011-07-01' then 'Before_FY12'
        when min(start_date) between '2011-07-01' and '2012-06-30' then 'FY12'
        when min(start_date) between '2012-07-01' and '2013-06-30' then 'FY13'
        when min(start_date) between '2013-07-01' and '2014-06-30' then 'FY14'
        when min(start_date) between '2014-07-01' and '2014-09-30' then 'FY15_Q1'
        when min(start_date) between '2014-10-01' and '2014-12-31' then 'FY15_Q2'
        when min(start_date) between '2015-01-01' and '2015-03-31' then 'FY15_Q3'
        when min(start_date) between '2015-04-01' and '2015-06-30' then 'FY15_Q4'
        when min(start_date) between '2015-07-01' and '2015-09-30' then 'FY16_Q1'
        when min(start_date) between '2015-10-01' and '2015-12-31' then 'FY16_Q2'
        when min(start_date) between '2016-01-01' and '2016-03-31' then 'FY16_Q3'
        when min(start_date) between '2016-04-01' and '2016-06-30' then 'FY16_Q4'
end as start_period
from license 
where platform = 'Server'
and license_level = 'Full'
group by tech_email_domain
--, base_product, sen
) 

select a.start_period, count(distinct a.tech_email_domain)
from first_date as a
left join license as b on a.tech_email_domain = b.tech_email_domain
where platform = 'Server'
and license_level = 'Full'
and b.expiry_date > '2016-05-22'
group by 1



-- code to view full results
select 
distinct a.tech_email_domain, a.base_product, b.base_product, a.min_date, max(b.expiry_date)
from first_date as a
left join license as b on a.tech_email_domain = b.tech_email_domain
where platform = 'Server'
and a.start_period = 'FY15_Q3'
and b.expiry_date > (a.min_date + 365)
group by 1,2,3,4 
order by 1,2,3,4

--code to get the count
select 
        a.base_product, 
        count(distinct a.tech_email_domain)
from first_date as a
left join license as b on a.tech_email_domain = b.tech_email_domain
where   platform = 'Server'
        and a.start_period = 'FY15_Q3'
        and b.expiry_date > (a.min_date + 365) --hide this line to get the total number acquired in the period.
group by 1 
order by 1


